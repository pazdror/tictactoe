# import pytest
# import json
# from src.authBE.auth_server import app as flask_app
#
#
# @pytest.fixture
# def app():
#     yield flask_app
#
#
# @pytest.fixture
# def client(app):
#     return app.test_client()
#
#
# @pytest.fixture
# def existing_user(app):
#     return {'username': 'test_user_id', 'user_alias': 'test_alias'}
#
#
# def test_login(app, client, existing_user):
#     res = client.get(f'api/login/{existing_user["username"]}')
#     assert res.status_code == 200
#     expected = existing_user
#     assert expected == json.loads(res.get_data(as_text=True))
#     res = client.get(f'api/login/ILLEGAL_CLIENT_NAME')
#     assert res.status_code == 401
#
#
# def test_signup_invalid_user_id(app, client):
#     res = client.post(f'/api/signup/INVALID_USER_ID')
#     expected = 'The email address is not valid. It must have exactly one @-sign.'
#     assert res.status_code == 406
#     assert expected == res.get_data(as_text=True)
#
#
# def test_signup_existing_user(app, client, existing_user):
#     res = client.post(f'/api/signup/{existing_user["username"]}')
#     expected = 'User ID already exist. Try another'
#     assert res.status_code == 401
#     assert expected == res.get_data(as_text=True)
#
#
# def test_signup_successful(app, client):
#     test_user = {'username': 'test_new_user_id@mail.com', 'user_alias': 'test_new_alias'}
#     client.delete(f'/api/remove/{test_user["username"]}')
#     res = client.post(f'/api/signup/{test_user["username"]}', data={'user_alias': test_user['user_alias']})
#     assert res.status_code == 200
#     assert json.loads(res.get_data(as_text=True)) == test_user
#
#     res = client.get(f'api/login/{test_user["username"]}')
#     assert res.status_code == 200
#     assert json.loads(res.get_data(as_text=True)) == test_user
#
#     res = client.delete(f'/api/remove/{test_user["username"]}')
#     assert res.status_code == 200
