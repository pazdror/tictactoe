__author__ = 'Dror Paz'
__createdOn__ = '2020/11/10'

from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from pathlib import Path
import json
import logging

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:////{Path(__file__).parent.absolute()}/gaming.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    alias = db.Column(db.String(120), unique=False, nullable=True)

    def __init__(self, username: str, alias: str = None):
        self.username = username
        self.alias = alias

    def __repr__(self):
        return f'<User {self.username}>'

    def serialize(self) -> str:
        d = dict(id=self.id, username=self.username, alias=self.alias)
        json_str = json.dumps(d)
        return json_str


class Game(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    game_name = db.Column(db.String(120), unique=False, nullable=False)
    game_json = db.Column(db.String(255), unique=False, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User', backref=db.backref('posts', lazy=True))

    def __init__(self, game_json: str, user_id: int, game_name: str = 'tictactoe'):
        self.game_json = game_json
        self.game_name = game_name
        self.user_id = user_id

    def serialize(self) -> str:
        d = dict(id=self.id, game_name=self.game_name, game_json=self.game_json, user_id=self.user_id)
        json_str = json.dumps(d)
        return json_str

    def __repr__(self):
        return f'<Game {self.game_name}:{self.id}> of player {self.user_id}'


def create_app():
    db.init_app(app)
    db.create_all()
    try:
        db.session.add(User(username='test_username', alias='test_alias'))
        db.session.commit()
    except exc.IntegrityError:
        pass
    return app


@app.route('/api/login/<username>', methods=['GET'])
def login(username: str):
    res = User.query.filter_by(username=username).first()
    if res:
        return res.serialize(), 200
    else:
        msg = 'User ID not found'
        logging.error(msg)
        return msg, 401


@app.route('/api/signup/<username>', methods=['POST'])
def signup(username):
    from email_validator import validate_email, EmailNotValidError
    res = User.query.filter_by(username=username).all()
    if res:
        msg = 'User ID already exist. Try another'
        logging.error(msg)
        return msg, 401
    else:
        try:
            validate_email(username)
        except EmailNotValidError as e:
            logging.error(str(e))
            return str(e), 406
    new_user = User(username=username, alias=request.form['alias'])
    db.session.add(new_user, _warn=False)
    db.session.commit()
    return new_user.serialize()


@app.route('/api/remove/<username>', methods=['DELETE'])
def remove(username: str):
    res = User.query.filter_by(username=username).first()
    if res:
        db.session.delete(res)
        db.session.commit()
        return f'Successfully deleted {username}', 200
    else:
        return 'Username not found', 401


@app.route('/api/save_game/<int:user_id>', methods=['POST'])
def save_game(user_id: int) -> tuple[str, int]:
    logging.debug(f'form is: {request.json}')
    game_name = request.json.get('game_name') or 'tictactoe'
    game_json = request.json['game_json']
    user = User.query.filter_by(id=user_id).first()
    logging.error(f'USER: {user}')
    if not user:
        msg = 'Could not find user for this game'
        logging.error(msg)
        return msg, 401
    game = Game(game_name=game_name, game_json=game_json, user_id=user.id)

    existing_game = Game.query.filter_by(user_id=user.id).first()
    logging.error(f'{existing_game}')
    if existing_game:
        existing_game.game_json = game_json
    else:
        db.session.add(game)
    db.session.commit()
    return 'Game saved', 200


@app.route('/api/get_game/<int:user_id>')
def get_game(user_id: int) -> dict[str:str, str:int]:
    '''
    Get saved game if exists, False otherwise
    '''
    game = Game.query.filter_by(user_id=user_id).first()
    if game:
        ret = {'game': game.game_json, 'game_id': game.id}
    else:
        ret = {'game': 'No game for this player', 'game_id': -1}
    return ret


@app.route('/api/delete_game/<int:game_id>', methods=['DELETE'])
def delete_game(game_id: int) -> tuple[str, int]:
    game = Game.query.filter_by(id=game_id).first()
    if game:
        db.session.delete(game)
        db.session.commit()
        return f'Successfully deleted game', 200
    else:
        return 'Game not found', 401

if __name__ == '__main__':
    app = create_app()
    app.run(host='0.0.0.0', port=5000, debug=True)
