__author__ = 'Dror Paz'
__createdOn__ = '2020/11/10'

import pytest
import json
from db import app as flask_app
from db import create_app



@pytest.fixture
def app():
    yield flask_app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def existing_user(app):
    return {'username': 'test_username', 'alias': 'test_alias'}


def test_login(app, client, existing_user):
    res = client.get(f'api/login/{existing_user["username"]}')
    assert res.status_code == 200
    expected = existing_user.items()
    result = json.loads(res.get_data(as_text=True)).items()
    assert set(expected).issubset(set(result))
    res = client.get(f'api/login/ILLEGAL_CLIENT_NAME')
    assert res.status_code == 401


def test_signup_invalid_user_id(app, client):
    res = client.post(f'/api/signup/INVALID_USER_ID')
    expected = 'The email address is not valid. It must have exactly one @-sign.'
    assert res.status_code == 406
    assert expected == res.get_data(as_text=True)


def test_signup_existing_user(app, client, existing_user):
    res = client.post(f'/api/signup/{existing_user["username"]}')
    expected = 'User ID already exist. Try another'
    assert res.status_code == 401
    assert expected == res.get_data(as_text=True)


def test_signup_successful(app, client):
    test_user = {'username': 'test_new_user_id@mail.com', 'alias': 'test_new_alias'}
    client.delete(f'/api/remove/{test_user["username"]}')
    res = client.post(f'/api/signup/{test_user["username"]}', data={'alias': test_user['alias']})
    assert res.status_code == 200
    expected = test_user.items()
    result = json.loads(res.get_data(as_text=True)).items()
    assert set(expected).issubset(set(result))

    res = client.get(f'api/login/{test_user["username"]}')
    assert res.status_code == 200
    result = json.loads(res.get_data(as_text=True))
    assert set(test_user).issubset(set(result))

    res = client.delete(f'/api/remove/{test_user["username"]}')
    assert res.status_code == 200
