import requests
import sys
from pathlib import Path
p = Path(__file__).parents[1]/'ticTacToeGame'
sys.path.insert(0, str(p))


def frontend():
    LoginScreen().main()


class GameManagement:
    def __init__(self, user):
        self.uri = 'http://game-manager-service:5000'
        self.choice_mapping = {1: self.play_tictactoe,
                               2: self.logout}
        self.user = user
        self.userid = user['id']
        self.username = user['username']
        self.alias = user['alias']
        self.game = None

    def main_choices(self) -> int:
        '''Display and receive user choices'''
        welcome_text = f'\n\nPlease enter your choice:\n' \
                       '1. Play TicTacToe\n' \
                       '2. Logout\n' \
                       '>>> '
        choice = input(welcome_text)

        try:
            choice = int(choice)
            return choice
        except ValueError:
            print('Must enter number')

    def main(self) -> int:
        '''
        Main loop. Accepts user choice and call actions
        '''
        print(f'\nWelcome {self.alias}!\n')
        action_code = self.main_choices()
        res = None
        if action_code in self.choice_mapping:
            res = self.choice_mapping.get(action_code)()
        else:
            print('\nINVALID CHOICE')

        if res == 'LOGOUT':
            return 200
        self.main()

    def play_tictactoe(self) -> int:
        # Get previous game or initiate a new one

        res = requests.get(f'{self.uri}/api/get_game/{self.userid}')
        game_json = res.json()['game']
        game_id = res.json()['game_id']

        # Run game
        game_state, game_json = TictactoeGame(game=game_json).main()

        # Deal with game persistence - Delete or save game as needed
        if game_state == 0:
            print('Saving game')
            res = requests.post(f'{self.uri}/api/save_game/{self.userid}', data={'game_json': game_json})
            print(res.text)
        else:
            if (game_id >= 0):
                requests.delete(f'{self.uri}/api/delete_game/{game_id}')

        return 200

    def logout(self):
        return 'LOGOUT'


class TictactoeGame:
    from tictactoe import TickTackToe
    def __init__(self, game=None):
        self.uri = 'http://tictactoe-service:5000'
        if game:
            self.game = TictactoeGame.TickTackToe.from_json(game)
        else:
            self.game = TictactoeGame.TickTackToe()
        self.user_index = self.game.current_turn_ind

    @staticmethod
    def pretty_board(board_list: (list, 'np.array')):
        '''
        Display formatted game board
        '''
        pretty_board = '|'.join(str(i).center(3) for i in board_list[:3])
        pretty_board += '\n' + '---+---+---' + '\n'
        pretty_board += '|'.join(str(i).center(3) for i in board_list[3:6])
        pretty_board += '\n' + '---+---+---' + '\n'
        pretty_board += '|'.join(str(i).center(3) for i in board_list[6:])
        return pretty_board

    def play_turn(self, location: int) -> int:
        '''Send turn to the game service, then check for tie or victory'''
        payload = {'game_json': self.game.to_json()}
        res = requests.post(f'{self.uri}/api/add_mark/{location}', json=payload)
        print(res.text)
        print(res.status_code)
        print(res.url)
        print(res.json())
        self.game = TictactoeGame.TickTackToe.from_json(res.json()['game_json'])
        game_state = self.game.game_state
        if game_state == 1:
            print(self.pretty_board(self.game.board))
            print('\n\nGAME OVER!\nYou got a tie!')
        elif game_state == 2:
            print(self.pretty_board(self.game.board))
            if self.game.current_turn_ind == self.user_index:
                print('\n\nCONGRATULATIONS! You won!')
            else:
                print('\n\nYou Lost!\n Why wont you try again?')
        return game_state

    def main_choices(self) -> int:
        welcome_text = '\n\nPlease choose location for your next mark using [0-8].\n' \
                       'Enter [9] to see the index of each location.\n' \
                       'Enter [10] to quit game (game state will be saved)\n' \
                       '>>> '
        choice = input(welcome_text)

        try:
            choice = int(choice)
            return choice
        except ValueError:
            'Must enter number'

    def main(self) -> tuple[int, str]:
        '''Main game loop. Send choice wherever it`s needed'''
        choice = 0
        while choice != 10:
            game_state = 0
            break_flag = False
            print(self.pretty_board(self.game.board))
            choice = self.main_choices()

            if choice in range(0, 9):
                game_state = self.play_turn(choice)
                if game_state:
                    break_flag = True
            elif choice == 9:
                print(self.pretty_board(range(0, 9)))
            elif choice == 10:
                break_flag = True
            else:
                print('\nINVALID CHOICE\n')

            if break_flag:
                return game_state, self.game.to_json()


class LoginScreen:
    def __init__(self):
        self.uri = 'http://db:5000'
        self.choice_mapping = {1: self.login,
                               2: self.signup,
                               3: exit}

    def main_choices(self) -> int:
        welcome_text = '\n\nWelcome to pazdror`s gaming service!\n' \
                       'Please enter your choice:\n' \
                       '1. Login\n' \
                       '2. Signup\n' \
                       '3. Exit\n' \
                       '>>>'
        choice = input(welcome_text)

        try:
            choice = int(choice)
            return choice
        except ValueError:
            'Must enter number'

    def login(self):
        username = input('Please enter user name (mail address): ')
        res = requests.get(f'{self.uri}/api/login/{username}')
        if res.status_code != 200:
            print(res.text)
        return res

    def signup(self):
        username = str(input('Please enter user name (mail address): '))
        alias = str(input('Please enter your desired alias: ') or username)
        res = requests.post(f'{self.uri}/api/signup/{username}', data={'alias': alias})
        if res.status_code != 200:
            print(res.text)
        return res

    def main(self):
        action_code = self.main_choices()
        res = None
        if action_code in self.choice_mapping:
            res = self.choice_mapping.get(action_code)()
        else:
            print('\nINVALID CHOICE')

        if res and res.status_code == 200:
            user = res.json()
            GameManagement(user=user).main()
        self.main()


if __name__ == '__main__':
    while True:
        pass
