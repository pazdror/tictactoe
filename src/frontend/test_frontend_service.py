__author__ = 'Dror Paz'
__createdOn__ = '2020/11/10'

import pytest
import frontend_service


def test_pretty_board():
    pretty_board = frontend_service.TictactoeGame.pretty_board(list(range(9)))
    expected = ' 0 | 1 | 2 \n---+---+---\n 3 | 4 | 5 \n---+---+---\n 6 | 7 | 8 '
    assert pretty_board == expected
