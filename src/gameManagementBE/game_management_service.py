__author__ = 'Dror Paz'
__createdOn__ = '2020/11/09'

from flask import Flask, request
import requests
import logging
import sys
from pathlib import Path
p = Path(__file__).parents[1]/'ticTacToeGame'
sys.path.insert(0, str(p))
from tictactoe import TickTackToe

app = Flask(__name__)

db_uri = 'http://db:5000'

@app.route('/')
def test():
    return 'game management service is ok'

@app.route('/api/get_game/<int:user_id>')
def get_game(user_id: int) -> dict[str:str, str:int]:
    res = requests.get(f'{db_uri}/api/get_game/{user_id}')
    if res.status_code != 200:
        return f'Error in db server: {res.text}', res.status_code
    game_meta = res.json()
    if game_meta['game'] == 'No game for this player':
        game_meta['game'] = TickTackToe().to_json()
    return game_meta


@app.route('/api/save_game/<user_id>', methods=['POST'])
def save_game(user_id: int):
    game_json = request.form['game_json']
    res = requests.post(f'{db_uri}/api/save_game/{user_id}', json={'game_json': game_json})
    if res.status_code != 200:
        msg = f'Error saving game: {res.text}'
        logging.error(msg)
        return msg, res.status_code
    return res.text


@app.route('/api/delete_game/<game_id>', methods=['DELETE'])
def delete_game(game_id):
    res = requests.delete(f'{db_uri}/api/delete_game/{game_id}')
    if res != 100:
        msg = f'Couldn`t delete game.\n{res.text}', res.status_code
        logging
    return 'Game deleted'


if __name__ == '__main__':
    app.run(host='0.0.0.0')
