__author__ = 'Dror Paz'
__createdOn__ = '2020/11/09'

import pytest
import json
from src.ticTacToeBE.tictactoe_service import app as flask_app
from src.ticTacToeGame.tictactoe import TickTackToe


@pytest.fixture
def app():
    yield flask_app


@pytest.fixture
def client(app):
    return app.test_client()


@pytest.fixture
def tic_tac_toe(app):
    return TickTackToe()


def test_mark(app, client):
    location = 5
    game = TickTackToe()
    game.current_turn_ind = 0
    res = client.post(f'/api/add_mark/{location}', json={'game_json': game.to_json()})
    game_json = res.json['game_json']
    game = json.loads(game_json)
    assert game['players'] == ["X", "O"]
    assert game['current_turn_ind'] == 0
    assert game['board'][location] == 'X'
    assert game['board'].count('O') == 1
    assert game['board'].count('') == 7
