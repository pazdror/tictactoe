__author__ = 'Dror Paz'
__createdOn__ = '2020/11/09'

from flask import Flask, request
import sys
from pathlib import Path
p = Path(__file__).parents[1]/'ticTacToeGame'
sys.path.insert(0, str(p))
from tictactoe import TickTackToe, tictactoe_auto_player

# Server
app = Flask(__name__)


def hello():
    return 'TicTacToe game service'


@app.route('/api/add_mark/<int:location>', methods=['POST'])
def add_mark(location: int):
    payload = request.get_json()
    game_json = payload['game_json']
    game = TickTackToe.from_json(game_json)
    if location in game.free_locations:
        game.add_mark(location)
        if not game.game_state:
            game = second_user_play(game)
    return {'game_json': game.to_json()}


def second_user_play(game: TickTackToe) -> TickTackToe:
    '''
    Normally, this function will send the game to the second user to play.
    In our case it runs the computer player
    '''
    return tictactoe_auto_player(game)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
