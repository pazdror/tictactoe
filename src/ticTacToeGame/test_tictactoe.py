__author__ = 'Dror Paz'
__createdOn__ = '2020/11/09'

import numpy as np
import pytest
from src.ticTacToeGame.tictactoe import TickTackToe, tictactoe_auto_player


@pytest.fixture
def game() -> TickTackToe:
    game = TickTackToe()
    game.current_turn_ind = 0
    return game


def test_autoplayer_add_second_mark(game):
    game.add_mark(4)
    game.add_mark(5)
    assert (game.board == ['', '', '', '', 'X', 'O', '', '', '']).all()
    game = tictactoe_auto_player(game)
    expected1 = {'board': ['', '', '', '', 'X', 'O', '', 'X', ''], 'players': ('X', 'O'), 'current_turn_ind': 1,
                 'game_state': 0}
    expected2 = {'board': ['', 'X', '', '', 'X', 'O', '', '', ''], 'players': ('X', 'O'), 'current_turn_ind': 1,
                 'game_state': 0}
    assert (expected1 == game.to_dict()) or (expected2 == game.to_dict())


def test_autoplayer_prevent_win(game):
    game.add_mark(4)
    game.add_mark(5)
    game.add_mark(7)
    game = tictactoe_auto_player(game)
    expected = {'board': ['', 'O', '', '', 'X', 'O', '', 'X', ''], 'players': ('X', 'O'), 'current_turn_ind': 0,
                'game_state': 0}
    assert expected == game.to_dict()


def test_autoplayer_win(game):
    game.add_mark(0)
    game.add_mark(3)
    game.add_mark(8)
    game.add_mark(5)
    game = tictactoe_auto_player(game)
    assert game.to_dict() == {'board': ['X', '', '', 'O', 'X', 'O', '', '', 'X'], 'players': ('X', 'O'),
                              'current_turn_ind': 0, 'game_state': 2}


def test_autoplayer_empty_board(game):
    game.current_turn_ind = 1
    game = tictactoe_auto_player(game)
    assert game.current_turn_ind == 0
    assert game.to_dict()['board'].count('O') == 1
    assert game.to_dict()['board'].count('') == 8


def test_is_tie(game):
    game.board = np.array(list('XXOOOXX') + [''] + ['O'])
    game.add_mark(7)
    expected = {'board': list('XXOOOXXXO'), 'players': ('X', 'O'),
                'current_turn_ind': 0, 'game_state': 1}
    assert game.to_dict() == expected
