import numpy as np
import json
import random


class TickTackToe:
    '''
    TicTacToe by Dror Paz
    ---------------------
    * To play your Turn use `add_mark(location)`
    * To view board's location indices use 'get_board_indices'
    * board_state is checked after every use of `add_mark`, before switching turns.

    * board_state values:
       {0: Ongoing game,
       1: Tie
       2: Victory
       }
    '''

    def __init__(self) -> None:
        self.board = np.full(9, '', dtype=str)
        self.players = ('X', 'O')
        self.current_turn_ind = random.getrandbits(1)
        self.game_state = 0
        self.lines_ind = [[0, 1, 2],
                          [3, 4, 5],
                          [6, 7, 8],
                          [0, 3, 6],
                          [1, 4, 7],
                          [2, 5, 8],
                          [0, 4, 8],
                          [2, 4, 6]]

    @property
    def free_locations(self) -> list:
        return(np.where(self.board=='')[0].tolist())

    @property
    def current_turn(self) -> str:
        '''returns the symbol of current player'''
        return self.players[self.current_turn_ind]

    def to_dict(self) -> dict:
        '''Serialize game to dict'''
        board = self.board.tolist()
        d = dict(board=board, players=self.players, current_turn_ind=self.current_turn_ind, game_state=self.game_state)
        return d

    def to_json(self) -> str:
        '''Serialize game to json string'''
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> 'TickTackToe':
        '''reload game from json string'''
        d = json.loads(json_str)
        required_keys = {'board', 'players', 'current_turn_ind'}
        assert required_keys.issubset(d.keys()), f'Some of the keys {required_keys} are missing in json: {d.keys()}'
        game = TickTackToe()
        game.board = np.array(d['board'])
        game.players = d['players']
        game.current_turn_ind = d['current_turn_ind']
        game.set_board_state()
        return game

    @staticmethod
    def _is_winning_line(line: np.array) -> bool:
        winning_line = False
        if (len(set(line)) == 1) and line[0]:
            winning_line = True
        return winning_line

    def is_winning_board(self) -> bool:
        for line in self.lines_ind:
            if self._is_winning_line(self.board[line]):
                return True
        return False

    def _is_board_full(self) -> bool:
        board_full = False
        if '' not in self.board:
            board_full = True
        return board_full

    def set_board_state(self) -> int:
        '''Check the board status for win\tie.
        See init docstring for options
        '''
        if self.is_tie():
            self.game_state = 1
        elif self.is_winning_board():
            self.game_state = 2
        else:
            self.game_state = 0
        return self.game_state

    def _advance_turn(self):
        '''move to next player'''
        self.current_turn_ind = 1 - self.current_turn_ind

    def add_mark(self, index: int):
        '''Add mark of current user and pass the turn to next user'''
        # TODO: catch mark attempt when game already finished
        # TODO: prevent overwriting marks
        self.board[index] = self.current_turn
        if not self.set_board_state():
            self._advance_turn()

    def is_tie(self) -> bool:
        tie = False
        if self._is_board_full() and (not self.is_winning_board()):
            tie = True
        return tie

    def get_square_board_state(self) -> np.array:
        '''Provide the board in a square (and not line) shape'''
        return self.board.reshape(3, 3)

    @staticmethod
    def get_board_indices() -> np.array:
        '''Provide reminder for the index of each location on the board'''
        return np.array(range(9)).reshape(3, 3)


def tictactoe_auto_player(game: TickTackToe) -> TickTackToe:
    '''Computer player for TicTacToe'''
    board = game.board
    if (board == '').all():  # check if board empty
        game.add_mark(random.randrange(9))
        return game

    else:
        for ind_line in game.lines_ind:  # check if it can win
            if ('' in board[ind_line]) and (sum(board[ind_line] == game.current_turn) == 2):
                line_mark = int(np.where(board[ind_line] == '')[0])
                game.add_mark(ind_line[line_mark])
                return game

        for ind_line in game.lines_ind:  # check to prevent opponent win
            if (sum(board[ind_line] == '') == 1) and (game.current_turn not in board[ind_line]):
                line_mark = np.where(board[ind_line] == '')[0][0]
                game.add_mark(ind_line[line_mark])
                return game

        for ind_line in game.lines_ind:  # try to have 2 in a line
            if (game.current_turn in board[ind_line]) and (sum(board[ind_line] == '') == 2):
                available_spots = [ind_line[i] for i in np.where(board[ind_line] == '')[0]]
                game.add_mark(random.choice(available_spots))
                return game

    # just put the mark somewhere free:
    free_places, = np.where(board == '')
    game.add_mark(random.choice(free_places))
    return game
